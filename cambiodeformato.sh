#!/bin/bash

# listar todos los archivos con extensión .mp4
archivos=$(ls *.mp4)

for archivo in $archivos; do
        # repite la accion renombrando el archivo a .mkv
        # el comando ffmpeg con el flag -i hace la conversión
    ffmpeg -i "$archivo" "${archivo%.mp4}.mkv"
done

ls -1 *.mkv
