#!/bash/bin
#
# lista todas imagenes que terminan con .jpeg
# y le cambia de formato hacia .png manteniendo el nombre
# original del archivo.

read -p "Que formato de imagen quieres cambiar?
Escribe solo el formato: " formato1
echo "----------------------------"
read -p "Escribe solo el formato
al que quieres cambiar: " formato2
echo "----------------------------"
for imagen in *.$formato1 ; do
        convert "$imagen" "${imagen%.$formato1}.$formato2"
done
echo -e "Los Archivos cambiados\n  son los siguientes:"
echo "----------------------------"
ls *.$formato2 | cat -n

